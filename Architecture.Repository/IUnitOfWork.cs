﻿namespace Architecture.Repository
{
    public interface IUnitOfWork
    {
        AppDbContext AppContext { get; set; }
        void Begin();
        void Commit();
        void RollBack();
    }
}
