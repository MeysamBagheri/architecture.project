﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Architecture.Domain;
using Microsoft.EntityFrameworkCore;
using Architecture.Core;
using Architecture.Core.Enum;

namespace Architecture.Repository
{
    public class BaseRepository<TEntity, TKey> : IBaseRepository<TEntity, TKey>
        where TEntity : AggregateRoot<TKey>
    {
        protected readonly IUnitOfWork _unitOfWork;
        public BaseRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync(SimpleQuery simpleQuery,
            Expression<Func<TEntity, bool>> condition = null)
        {
            return await GetAsync(simpleQuery, condition, null, null);
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync(SimpleQuery simpleQuery,
            Expression<Func<TEntity, bool>> condition = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderer = null,
            string includes = null, params Expression<Func<TEntity, object>>[] includeParams)
        {
            IQueryable<TEntity> query = _unitOfWork.AppContext.Set<TEntity>()
                .AsNoTracking()
                .OrderByDescending(x => x.CreatedDate)
                .AsQueryable();

            return await query
                .SetGlobalFilters<TEntity, TKey>()
                .SetIncludes(includes, includeParams)
                .SetConditions(condition)
                .SetOrder(orderer)
                .SetPagination(simpleQuery)
                .ToListAsync();
        }

        public virtual async Task<long> GetCountAsync(Expression<Func<TEntity, bool>> condition = null)
        {
            IQueryable<TEntity> query = _unitOfWork.AppContext.Set<TEntity>()
                .AsNoTracking();

            return await query
                .SetGlobalFilters<TEntity, TKey>()
                .SetConditions(condition)
                .LongCountAsync();
        }

        public virtual IQueryable<TEntity> GetQueryable(SimpleQuery simpleQuery,
            Expression<Func<TEntity, bool>> condition = null)
        {
            return GetQueryable(simpleQuery, condition, null, null);
        }

        public virtual IQueryable<TEntity> GetQueryable(SimpleQuery simpleQuery,
            Expression<Func<TEntity, bool>> condition = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderer = null, string includes = null,
            params Expression<Func<TEntity, object>>[] includeParams)
        {
            IQueryable<TEntity> query = _unitOfWork.AppContext.Set<TEntity>()
                .AsNoTracking()
                .OrderByDescending(x => x.CreatedDate)
                .AsQueryable();

            return query
                .SetGlobalFilters<TEntity, TKey>()
                .SetIncludes(includes, includeParams)
                .SetConditions(condition)
                .SetOrder(orderer)
                .SetPagination(simpleQuery);
        }

        public virtual async Task<TEntity> GetByIdAsync(TKey id)
        {
            return await _unitOfWork.AppContext.Set<TEntity>().FindAsync(id);
        }

        public virtual async Task<TEntity> GetByIdAsync(TKey id, string includes = null,
            params Expression<Func<TEntity, object>>[] includeParams)
        {
            var query = _unitOfWork.AppContext.Set<TEntity>().AsQueryable();

            return await query
                .SetIncludes(includes, includeParams)
                .SetGlobalFilters<TEntity, TKey>()
                .FirstAsync(x => x.Id.ToString() == id.ToString());
        }

        public virtual async Task<TKey> AddAsync(TEntity entity, long userId)
        {
            entity.CreateIt(userId);
            await _unitOfWork.AppContext.Set<TEntity>().AddAsync(entity);
            await _unitOfWork.AppContext.SaveChangesAsync();
            return entity.Id;
        }

        public virtual async Task<int> UpdateAsync(TEntity entity, long userId)
        {
            entity.ModifyIt(userId);
            _unitOfWork.AppContext.Set<TEntity>().Update(entity);
            var editing = _unitOfWork.AppContext.Set<TEntity>().First(x => x.Id.ToString() == entity.Id.ToString());
            _unitOfWork.AppContext.Entry(editing).CurrentValues.SetValues(entity);
            return await _unitOfWork.AppContext.SaveChangesAsync();
        }

        public virtual async Task<bool> ActiveAsync(TKey id, long userId)
        {
            var entity = await _unitOfWork.AppContext.Set<TEntity>()
                .FirstOrDefaultAsync(x => x.Id.ToString() == id.ToString());
            entity?.ActiveIt(userId);
            return await _unitOfWork.AppContext.SaveChangesAsync() > 0;
        }

        public virtual async Task<bool> DeActiveAsync(TKey id, long userId)
        {
            var entity = await _unitOfWork.AppContext.Set<TEntity>()
                .FirstOrDefaultAsync(x => x.Id.ToString() == id.ToString());
            entity?.DeActiveIt(userId);
            return await _unitOfWork.AppContext.SaveChangesAsync() > 0;
        }

        public virtual async Task<bool> DeleteAsync(TKey id, DeletingType deletingType, long userId)
        {
            switch (deletingType)
            {
                case DeletingType.SoftDelete:
                    var softEntity = await _unitOfWork.AppContext.Set<TEntity>()
                        .FirstOrDefaultAsync(x => x.Id.ToString() == id.ToString());
                    softEntity?.DeleteIt(userId);
                    return await _unitOfWork.AppContext.SaveChangesAsync() > 0;

                case DeletingType.HardDelete:
                    var hardEntity = await _unitOfWork.AppContext.Set<TEntity>().FindAsync(id);
                    var result = _unitOfWork.AppContext.Entry(hardEntity).State == EntityState.Detached
                        ? _unitOfWork.AppContext.Attach(hardEntity)
                        : _unitOfWork.AppContext.Set<TEntity>().Remove(hardEntity);
                    return await _unitOfWork.AppContext.SaveChangesAsync() > 0;
                default:
                    return await _unitOfWork.AppContext.SaveChangesAsync() > 0;
            }
        }

        public virtual async Task<int> AddRangeAsync(IEnumerable<TEntity> entities, long userId)
        {
            var items = entities.ToList();
            items.ForEach(x => x.CreateIt(userId));
            await _unitOfWork.AppContext.Set<TEntity>().AddRangeAsync(items);
            return await _unitOfWork.AppContext.SaveChangesAsync();
        }

        public virtual async Task<int> UpdateRangeAsync(IEnumerable<TEntity> entities, long userId)
        {
            var items = entities.ToList();
            items.ForEach(x => x.ModifyIt());
            _unitOfWork.AppContext.Set<TEntity>().UpdateRange(items);
            return await _unitOfWork.AppContext.SaveChangesAsync();
        }

        public virtual async Task<int> ActiveRangeAsync(IEnumerable<TKey> ids, long userId)
        {
            var items = await _unitOfWork.AppContext.Set<TEntity>().Where(x => ids.Contains(x.Id)).ToListAsync();
            items.ForEach(x => x.ActiveIt());
            return await UpdateRangeAsync(items, userId);
        }

        public virtual async Task<int> DeActiveRangeAsync(IEnumerable<TKey> ids, long userId)
        {
            var items = await _unitOfWork.AppContext.Set<TEntity>().Where(x => ids.Contains(x.Id)).ToListAsync();
            items.ForEach(x => x.DeActiveIt());
            return await UpdateRangeAsync(items, userId);
        }

        public virtual async Task<int> DeleteRangeAsync(IEnumerable<TKey> ids, DeletingType deletingType, long userId)
        {
            var items = await _unitOfWork.AppContext.Set<TEntity>().Where(x => ids.Contains(x.Id)).ToListAsync();
            _unitOfWork.AppContext.Set<TEntity>().RemoveRange(items);
            return await _unitOfWork.AppContext.SaveChangesAsync();
        }

        public virtual async Task<int> SaveChangesAsync()
        {
            return await _unitOfWork.AppContext.SaveChangesAsync();
        }
    }

    public static class BaseRepositoryExtension
    {
        public static IQueryable<TEntity> SetGlobalFilters<TEntity, TKey>(this IQueryable<TEntity> query)
            where TEntity : AggregateRoot<TKey>
        {
            return query.Where(x => x.IsActive && !x.IsDelete);
        }

        public static IQueryable<TEntity> SetPagination<TEntity>(this IQueryable<TEntity> query,
          SimpleQuery simpleQuery)
        {
            query = simpleQuery.PageOffset != null && simpleQuery.PageLimit != null && simpleQuery.PageOffset > 0 && simpleQuery.PageLimit > 0
                ? query.Skip(((int)simpleQuery.PageOffset) * (int)simpleQuery.PageLimit).Take((int)simpleQuery.PageLimit)
                : query;
            return query;
        }

        public static IQueryable<TEntity> SetIncludes<TEntity>(this IQueryable<TEntity> query, string includes,
            params Expression<Func<TEntity, object>>[] includeParams)
            where TEntity : class
        {
            if (includes != null)
                includes?.Split(',').ToList()?.ForEach(include => query.Include(include));

            if (includeParams != null)
                query = includeParams.Aggregate(query, (current, include) => current.Include(include));

            return query;
        }

        public static IQueryable<TEntity> SetOrder<TEntity>(this IQueryable<TEntity> query,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderer)
        {
            query = orderer != null ? orderer(query) : query;
            return query;
        }

        public static IQueryable<TEntity> SetConditions<TEntity>(this IQueryable<TEntity> query,
            Expression<Func<TEntity, bool>> condition)
        {
            query = query.Where(condition ?? (x => true));
            return query;
        }
    }
}
