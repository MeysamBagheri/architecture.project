﻿using Architecture.Domain;
using Architecture.Domain.Models;
using Architecture.Repository.Configs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Architecture.Repository
{
    public class AppDbContext : DbContext
    {
        private readonly IConfiguration _configuration;

        public AppDbContext(DbContextOptions<AppDbContext> options, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "Server=MSSQLSERVER; User ID=sa;Password=123; Database =ArchitectureDb;";
            //_configuration.GetSection("CurrentConnection").Value;
            optionsBuilder
               .UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60))
               .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        }

        public virtual DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
