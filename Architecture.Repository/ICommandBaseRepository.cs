﻿using System.Threading.Tasks;

namespace Architecture.Domain
{
    public interface ICommandBaseRepository<in TEntity> : IBaseRepositoryOld where TEntity : class
    {
        Task<long> Add(TEntity entity, long userId);
        Task<long> Update(TEntity entity, long userId);
        Task<bool> Active(long id, long userId);
        Task<bool> DeActive(long id, long userId);
        Task<bool> Delete(long id, bool softDelete, long userId);
    }
}
