﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace Architecture.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        public AppDbContext AppContext { get; set; }
        public UnitOfWork(AppDbContext context)
        {
            AppContext = context;
        }

        private TransactionScope _transactionScope;
        public void Begin()
        {
            _transactionScope = new TransactionScope(
                TransactionScopeOption.Required,
                new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted },
                TransactionScopeAsyncFlowOption.Enabled);
        }
        public void Commit()
        {
            _transactionScope.Complete();
            _transactionScope.Dispose();
        }
        public void RollBack()
        {
            _transactionScope?.Dispose();
        }
    }
}
