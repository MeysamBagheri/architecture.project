﻿using Architecture.Core;
using Architecture.Core.Enum;
using Architecture.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Architecture.Repository
{
    public interface IBaseRepository<TEntity, TKey>
        where TEntity : AggregateRoot<TKey>
    {
        Task<IEnumerable<TEntity>> GetAsync(SimpleQuery simpleQuery,
            Expression<Func<TEntity, bool>> condition = null);

        Task<IEnumerable<TEntity>> GetAsync(SimpleQuery simpleQuery,
            Expression<Func<TEntity, bool>> condition = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderer = null,
            string includes = null, params Expression<Func<TEntity, object>>[] includeParams);

        Task<long> GetCountAsync(Expression<Func<TEntity, bool>> condition = null);

        IQueryable<TEntity> GetQueryable(SimpleQuery simpleQuery,
            Expression<Func<TEntity, bool>> condition = null);

        IQueryable<TEntity> GetQueryable(SimpleQuery simpleQuery, Expression<Func<TEntity, bool>> condition = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderer = null, string includes = null,
            params Expression<Func<TEntity, object>>[] includeParams);

        Task<TEntity> GetByIdAsync(TKey id);

        Task<TEntity> GetByIdAsync(TKey id, string includes = null,
            params Expression<Func<TEntity, object>>[] includeParams);

        Task<TKey> AddAsync(TEntity entity, long userId);
        Task<int> AddRangeAsync(IEnumerable<TEntity> entities, long userId);

        Task<int> UpdateAsync(TEntity entity, long userId);
        Task<int> UpdateRangeAsync(IEnumerable<TEntity> entities, long userId);

        Task<bool> ActiveAsync(TKey id, long userId);
        Task<int> ActiveRangeAsync(IEnumerable<TKey> ids, long userId);

        Task<bool> DeActiveAsync(TKey id, long userId);
        Task<int> DeActiveRangeAsync(IEnumerable<TKey> ids, long userId);

        Task<bool> DeleteAsync(TKey id, DeletingType deletingType, long userId);
        Task<int> DeleteRangeAsync(IEnumerable<TKey> ids, DeletingType deletingType, long userId);

        Task<int> SaveChangesAsync();
    }
}
