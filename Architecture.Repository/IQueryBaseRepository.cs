﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Architecture.Domain
{
    public interface IQueryBaseRepository<TEntity> : IBaseRepositoryOld
        where TEntity : class
    {
        Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> condition = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderer = null,
            string includes = null, int offset = 0, int limit = 0);

        IQueryable<TEntity> GetQueryable(Expression<Func<TEntity, bool>> condition = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderer = null,
            string includes = null, int offset = 0, int limit = 0);

        Task<TEntity> GetById(object id);
    }
}
