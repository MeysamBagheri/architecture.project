﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Architecture.Core
{
    public class SimpleQuery : ISimpleQuery
    {
        public int? PageOffset { get; set; } = 0;
        public int? PageLimit { get; set; } = 10;
        public string Sort { get; set; }
        public string Filter { get; set; }
    }
}
