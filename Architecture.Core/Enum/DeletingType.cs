﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Architecture.Core.Enum
{
    public enum DeletingType
    {
        SoftDelete = 0,
        HardDelete = 1
    }
}
