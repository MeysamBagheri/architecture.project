using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Architecture.Application;
using Architecture.Application.Validations;
using Architecture.Application.ViewModels;
using Architecture.Domain;
using Architecture.Domain.Models;
using Architecture.Presentation.Api.Extensions.Log;
using Architecture.Repository;
using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace ArchitectureProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped(typeof(IBaseRepository<,>), typeof(BaseRepository<,>));
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Default"));
            });
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IBaseService<,,,>), typeof(BaseService<,,,>));
            services.AddEntityFrameworkSqlServer();
            services.AddHttpContextAccessor();
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.ASCII.GetBytes(
                        Configuration.GetSection("JwtConfig:secret").Value)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
            services.AddAutoMapperConfigs();
            services.AddMvc().AddFluentValidation(fv =>
                {
                    fv.ImplicitlyValidateChildProperties = true;
                    fv.RegisterValidatorsFromAssemblyContaining<UserValidator>();
                });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => endpoints.MapControllers());
            app.UseCors("CorsPolicy");
            app.UseStaticFiles();
            app.UseLogMiddleware();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }
    }

    public static class RequestLoggingMiddlewareExtension
    {
        public static IApplicationBuilder UseLogMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<RequestLoggingMiddleware>();
            return app;
        }
    }

    public static class AddAutoMapperConfigsExtension
    {
        public static IServiceCollection AddAutoMapperConfigs(this IServiceCollection services)
        {
            var viewModelTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(IBaseViewModel).IsAssignableFrom(p)).OrderBy(x => x.Name)
                .Except(new List<Type>() { typeof(IBaseViewModel), typeof(BaseViewModel<>) })
                .ToList();

            var aggregateTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(IAggregateRoot).IsAssignableFrom(p)).OrderBy(x => x.Name)
                .Except(new List<Type>() { typeof(IAggregateRoot), typeof(AggregateRoot<>) })
                .ToList();

            var commandTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(IBaseCommand).IsAssignableFrom(p)).OrderBy(x => x.Name)
                .Except(new List<Type>() { typeof(IBaseCommand), typeof(BaseCommand) })
                .ToList();

            var viewModelAggregateMapping = aggregateTypes.Select((k, i) =>
                new
                {
                    k,
                    v = viewModelTypes.Where(x => k.Name == x.Name.Replace("ViewModel", ""))
                    .ToList()
                })
               .ToDictionary(x => x.k, x => x.v);

            var commandAggregateMapping = commandTypes.Select((k, i) =>
                new
                {
                    k,
                    v = aggregateTypes.Where(x =>
                           x.Name == k.Name.Replace("ViewModel", "") ||
                           x.Name == k.Name.Replace("CommandDto", "") ||
                           x.Name == k.Name.Replace("Command", "") ||
                           x.Name == k.Name.Replace("Dto", ""))
                    .ToList()
                })
               .ToDictionary(x => x.k, x => x.v);

            services.AddSingleton(new MapperConfiguration(cfg =>
            {
                foreach (var mapItem in viewModelAggregateMapping)
                    mapItem.Value.ForEach(mapDestination => cfg.CreateMap(mapItem.Key, mapDestination));

                foreach (var mapItem in commandAggregateMapping)
                    mapItem.Value.ForEach(mapDestination => cfg.CreateMap(mapItem.Key, mapDestination));
            }).CreateMapper());
            return services;
        }
    }
}
