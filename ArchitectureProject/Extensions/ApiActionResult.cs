﻿using Architecture.Application.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Architecture.Presentation.Api.Extensions
{
    public class ApiResult : IActionResult
    {
        private readonly IServiceResponse _serviceResponse;
        public ApiResult(IServiceResponse serviceResponse)
        {
            _serviceResponse = serviceResponse;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            var response = new ObjectResult(_serviceResponse)
            {
                StatusCode = SetStatusCode(_serviceResponse)
            };

            await response.ExecuteResultAsync(context);
        }

        public int SetStatusCode(IServiceResponse serviceResponse)
        {
            if (serviceResponse == null)
                return StatusCodes.Status400BadRequest;

            if (serviceResponse.ResultStatus == ResultStatus.Exception)
                return StatusCodes.Status400BadRequest;

            if (serviceResponse.Data == null)
                return StatusCodes.Status204NoContent;

            return StatusCodes.Status200OK;
        }
    }
}
