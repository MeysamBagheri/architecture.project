﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace Architecture.Presentation.Api.Extensions.Jwt
{
    public static class CurrentUser
    {
        public static long CurrentUserId(this IHttpContextAccessor httpContextAccessor)
        {
            var userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);
            if (userId != null)
                return Convert.ToInt32(userId);
            else
                return -1;
        }
    }
}
