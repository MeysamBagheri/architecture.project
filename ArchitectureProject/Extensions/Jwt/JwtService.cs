﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Architecture.Application.ViewModels;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Architecture.Presentation.Api.Extensions.Jwt
{
    public class JwtService
    {
        private readonly string _secret;
        private readonly int _expDate;

        public JwtService(IConfiguration configuration)
        {
            _secret = configuration.GetSection("JwtConfig").GetSection("secret").Value;
            _expDate = Convert.ToInt32(configuration.GetSection("JwtConfig").GetSection("expirationInDays").Value);
        }

        public UserViewModel GenerateSecurityToken(object model)
        {
            var user = (UserViewModel)model;
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    //new Claim(ClaimTypes.UserData, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(_expDate),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            return user;
        }
    }
}
