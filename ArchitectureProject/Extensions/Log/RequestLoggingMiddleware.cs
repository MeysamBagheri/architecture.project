﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Architecture.Presentation.Api.Extensions.Log
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        //private readonly NLog.Logger _nLogger;
        public RequestLoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<RequestLoggingMiddleware>();
            //_nLogger = NLog.Config.LoggingConfiguration .NLogBuilder.ConfigureNLog("Nlog.config").GetCurrentClassLogger();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            finally
            {
                //log request 

                //_logger.LogInformation(
                //    "Request {method} {url} => {statusCode}",
                //    context.Request?.Method,
                //    context.Request?.Path.Value,
                //    context.Response?.StatusCode);
            }
        }
    }

    public static class RequestLoggingMiddlewareExtension
    {
        public static IApplicationBuilder UseLogMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<RequestLoggingMiddleware>();
            return app;
        }
    }

}
