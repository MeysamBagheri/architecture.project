﻿using Architecture.Presentation.Api.Extensions.Jwt;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;

namespace Architecture.Api.Controllers
{
    [ApiController]
    [CustomAuthorize]
    public class BaseApiController : ControllerBase
    {
        public BaseApiController() : base()
        {
        }

        public ClaimsPrincipal CurrentUser
        {
            get { return HttpContext.User; }
            set { _ = HttpContext.User; }
        }

        public long UserId
        {
            get
            {
                try
                {
                    var id = HttpContext?.User?.Identity?.Name;
                    if (id != null)
                        return int.Parse(id);
                    else return -1;
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }


        //public string UserName => GetValueFromClaims("UserName");
        //public List<string> Roles
        //{
        //    get
        //    {
        //        var roles = GetValueFromClaims("Roles");
        //        return roles?.Split(',')?.ToList();
        //    }
        //}
        //public bool IsInRole(string role)
        //{
        //    if (this.Roles != null && this.Roles.Any() && this.Roles.Contains(role))
        //        return true;
        //    else
        //        return false;
        //}
        //public bool IsInRole(RolesEnum role)
        //{
        //    if (this.Roles.Any() && this.Roles.Contains(role.ToString()))
        //        return true;
        //    else
        //        return false;
        //}

        private string GetValueFromClaims(string key)
        {
            try
            {
                var value = ((ClaimsIdentity)HttpContext.User.Identity)?.FindFirst(key)?.Value;

                if (value != null)
                    return value.ToString();
                else
                    return string.Empty;
            }
            catch
            {
                return null;
            }
        }
    }
}