﻿using Architecture.Api.Extensions;
using Architecture.Application;
using Architecture.Application.ViewModels;
using Architecture.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace Architecture.Api.Controllers
{
    [Route("api/samples")]
    public class SampleController : IGenericController<User, long, UserViewModel, UserViewModel>
    {
        public SampleController(IBaseService<User, long, UserViewModel, UserViewModel> baseService) : base(baseService)
        {

        }
    }
}