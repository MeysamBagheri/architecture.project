﻿using Architecture.Application;
using Architecture.Application.Queries;
using Architecture.Application.Validations;
using Architecture.Application.ViewModels;
using Architecture.Core.Enum;
using Architecture.Domain.Models;
using Architecture.Presentation.Api.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Architecture.Api.Controllers
{
    [Route("api/users")]
    [AllowAnonymous]
    public class UserController : BaseApiController
    {
        private readonly IBaseService<User, long, UserCommandDto, UserViewModel> _userService;
        public UserController(IBaseService<User, long, UserCommandDto, UserViewModel> userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]UserQuery model)
        {
            var response = await _userService.GetAsync(model);
            return new ApiResult(response);
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<IActionResult> GetById(long id)
        {
            var response = await _userService.GetByIdAsync(id);
            return new ApiResult(response);
        }

        [HttpPost]
        public async Task<IActionResult> Add(UserCommandDto model)
        {
            var response = await _userService.AddAsync(model, this.UserId);
            return new ApiResult(response);
        }

        [HttpPut]
        public async Task<IActionResult> Update(UserCommandDto model)
        {
            var response = await _userService.UpdateAsync(model, this.UserId);
            return new ApiResult(response);
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(long id, DeletingType deletingType)
        {
            var response = await _userService.DeleteAsync(id, deletingType, this.UserId);
            return new ApiResult(response);
        }

        [Route("active/{id}")]
        [HttpPatch]
        public async Task<IActionResult> Active(long id)
        {
            var response = await _userService.ActiveAsync(id, this.UserId);
            return new ApiResult(response);
        }

        [Route("deactive/{id}")]
        [HttpPatch]
        public async Task<IActionResult> DeActive(long id)
        {
            var response = await _userService.DeActiveAsync(id, this.UserId);
            return new ApiResult(response);
        }
    }
}
