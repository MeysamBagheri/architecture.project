﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Architecture.Api.Controllers;
using Architecture.Application;
using Architecture.Application.Queries;
using Architecture.Core;
using Architecture.Core.Enum;
using Architecture.Domain;
using Architecture.Presentation.Api.Extensions;
using Architecture.Presentation.Api.Extensions.Jwt;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Architecture.Api.Extensions
{
    [Route("api/[controller]")]
    public abstract class IGenericController<TEntity, TKey, TCommand, TViewModel> : BaseApiController
        where TCommand : IBaseCommand
        where TViewModel : IBaseViewModel
        where TEntity : IAggregateRoot
    {
        private readonly IBaseService<TEntity, TKey, TCommand, TViewModel> _baseService;
        public IGenericController(IBaseService<TEntity, TKey, TCommand, TViewModel> baseService)
        {
            _baseService = baseService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]SimpleQuery model)
        {
            var response = await _baseService.GetAsync(model);
            return new ApiResult(response);
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<IActionResult> GetById(TKey id)
        {
            var response = await _baseService.GetByIdAsync(id);
            return new ApiResult(response);
        }

        [HttpPost]
        public async Task<IActionResult> Add(TCommand model)
        {
            var response = await _baseService.AddAsync(model, this.UserId);
            return new ApiResult(response);
        }

        [HttpPut]
        public async Task<IActionResult> Update(TCommand model)
        {
            var response = await _baseService.UpdateAsync(model, this.UserId);
            return new ApiResult(response);
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(TKey id, DeletingType deletingType)
        {
            var response = await _baseService.DeleteAsync(id, deletingType, this.UserId);
            return new ApiResult(response);
        }

        [Route("active/{id}")]
        [HttpPatch]
        public async Task<IActionResult> Active(TKey id, DeletingType deletingType)
        {
            var response = await _baseService.ActiveAsync(id, this.UserId);
            return new ApiResult(response);
        }

        [Route("deactive/{id}")]
        [HttpPatch]
        public async Task<IActionResult> DeActive(TKey id, DeletingType deletingType)
        {
            var response = await _baseService.DeActiveAsync(id, this.UserId);
            return new ApiResult(response);
        }
    }
}