﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Architecture.Domain
{
    public class BaseEntity<TKey> : IBaseEntity
    {
        [Key]
        [Required]
        public TKey Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}