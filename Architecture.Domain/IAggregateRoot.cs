﻿namespace Architecture.Domain
{
    public interface IAggregateRoot : IBaseEntity
    {
        void ActiveIt(long? userId = 0, string Ip = null);
        void DeActiveIt(long? userId = 0, string Ip = null);
        void CreateIt(long? userId = 0, string Ip = null);
        void ModifyIt(long? userId = 0, string Ip = null);
        void DeleteIt(long? userId = 0, string Ip = null);
    }
}
