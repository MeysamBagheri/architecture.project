﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Architecture.Domain
{
    public class AggregateRoot<TKey> : BaseEntity<TKey>, IAggregateRoot
    {
        public bool IsActive { get; set; } = true;
        public bool IsDelete { get; set; } = false;
        public long? CreatedBy { get; set; }
        public long? DeletedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public string IP { get; set; }

        public void ActiveIt(long? userId = 0, string Ip = null)
        {
            this.IsActive = true;
            this.ModifiedBy = userId;
            this.ModifiedDate = DateTime.Now;
            this.IP = Ip;
        }
        public void DeActiveIt(long? userId = 0, string Ip = null)
        {
            this.IsActive = false;
            this.ModifiedBy = userId;
            this.ModifiedDate = DateTime.Now;
            this.IP = Ip;
        }
        public void CreateIt(long? userId = 0, string Ip = null)
        {
            this.CreatedBy = userId;
            this.CreatedDate = DateTime.Now;
            this.IP = Ip;
        }
        public void ModifyIt(long? userId = 0, string Ip = null)
        {
            this.ModifiedBy = userId;
            this.ModifiedDate = DateTime.Now;
            this.IP = Ip;
        }
        public void DeleteIt(long? userId = 0, string Ip = null)
        {
            this.IsDelete = true;
            this.DeletedBy = userId;
            this.DeletedDate = DateTime.Now;
            this.IP = Ip;
        }
    }
}
