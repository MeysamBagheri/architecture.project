﻿using Architecture.Application.Infrastructure;
using Architecture.Application.ViewModels;
using Architecture.Core;
using Architecture.Core.Enum;
using Architecture.Domain;
using Architecture.Domain.Models;
using Architecture.Repository;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Architecture.Application
{
    public class BaseService<TEntity, TKey, TCommand, TViewModel>
        : IBaseService<TEntity, TKey, TCommand, TViewModel>
        where TCommand : BaseCommand
        where TViewModel : BaseViewModel<TKey>
        where TEntity : AggregateRoot<TKey>
    {
        private readonly IBaseRepository<TEntity, TKey> _baseRepository;
        private readonly IMapper _mapper;
        public BaseService(IBaseRepository<TEntity, TKey> baseRepository, IMapper mapper)
        {
            _mapper = mapper;
            _baseRepository = baseRepository;
        }

        public virtual async Task<ServiceResponse<PagenatedList<TViewModel>>> GetAsync(SimpleQuery simpleQuery)
        {
            var data = await _baseRepository.GetAsync(simpleQuery);
            return ServiceResponse<TViewModel>.Success(
                data.Select(x => _mapper.Map<TEntity, TViewModel>(x)).ToList(),
                await _baseRepository.GetCountAsync());
        }

        public virtual async Task<ServiceResponse<TViewModel>> GetByIdAsync(TKey id)
        {
            var data = await _baseRepository.GetByIdAsync(id);
            return ServiceResponse<TViewModel>.Success(_mapper.Map<TEntity, TViewModel>(data));
        }

        public virtual async Task<ServiceResponse<TKey>> AddAsync(TCommand command, long userId)
        {
            var mapped = _mapper.Map<TCommand, TEntity>(command);
            var result = await _baseRepository.AddAsync(mapped, userId);
            return ServiceResponse<TKey>.Success(result);
        }

        public virtual async Task<ServiceResponse<long>> UpdateAsync(TCommand command, long userId)
        {
            var mapped = _mapper.Map<TCommand, TEntity>(command);
            var result = await _baseRepository.UpdateAsync(mapped, userId);
            return ServiceResponse<long>.Success(result);
        }

        public virtual async Task<ServiceResponse<bool>> ActiveAsync(TKey id, long userId)
        {
            var result = await _baseRepository.ActiveAsync(id, userId);
            return ServiceResponse<bool>.Success(result);
        }

        public virtual async Task<ServiceResponse<bool>> DeActiveAsync(TKey id, long userId)
        {
            var result = await _baseRepository.DeActiveAsync(id, userId);
            return ServiceResponse<bool>.Success(result);
        }

        public virtual async Task<ServiceResponse<bool>> DeleteAsync(TKey id, DeletingType deletingType, long userId)
        {
            var result = await _baseRepository.DeleteAsync(id, deletingType, userId);
            return ServiceResponse<bool>.Success(result);
        }
    }
}
