﻿using Architecture.Domain;
using Architecture.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Architecture.Application.ViewModels
{
    public class UserViewModel : BaseViewModel<long>, IBaseCommand
    {
        public DateTime CreatedDate { get; set; }
        public string Token { get; set; }
    }

    public class UserCommandDto : BaseCommand
    {
        public DateTime CreatedDate { get; set; }
    }
}
