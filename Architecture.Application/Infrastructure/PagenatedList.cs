﻿using System.Collections.Generic;
using System.Linq;

namespace Architecture.Application.Infrastructure
{
    public class PagenatedList<T>
    {
        public long Total { get; set; }
        public List<T> Rows { get; set; } = new List<T>();
    }
}
