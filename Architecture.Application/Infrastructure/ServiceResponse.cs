﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Architecture.Application.Infrastructure
{
    public class ServiceResponse<T> : IServiceResponse
    {
        public ServiceResponse() { }

        [JsonProperty("Data")]
        public object Data { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("Exception")]
        public Exception Exception { get; private set; }

        [JsonProperty("ResultStatus")]
        public ResultStatus ResultStatus { get; set; }

        [JsonProperty("ExtraData")]
        public object ExtraData { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("ErrorCode")]
        public int ErrorCode { get; private set; }

        /// <summary>
        /// تعداد کل رکوردها
        /// </summary>
        public long RecordsCount { get; set; }

        /// <summary>
        /// اگر داده ای یافت نشود از این استفاده گردد
        /// </summary>
        /// <param name="value"></param>
        public void SetNotFound(T value)
        {
            Data = value;
            ResultStatus = ResultStatus.DataNotFound;
            Exception = null;
            Message = "داده ای یافت نشد";
        }

        /// <summary>
        /// اگر داده ای یافت نشود از این استفاده گردد
        /// </summary>
        /// <param name="value"></param>
        /// <param name="message"></param>
        public void SetNotFound(T value, string message)
        {
            Data = value;
            ResultStatus = ResultStatus.DataNotFound;
            Exception = null;
            Message = message;
        }

        /// <summary>
        /// برای مقدار دادن استفاده میگردد و مقدار درون این قرار میگیرد
        /// </summary>
        /// <param name="value"></param>
        public void SetData(T value)
        {
            SetData(value, string.Empty);
        }

        /// <summary>
        /// برای مقدار دادن استفاده میگردد و مقدار درون این قرار میگیرد
        /// </summary>
        /// <param name="value"></param>
        public void SetData(T value, string message)
        {
            Data = value;
            var typeParameterType = typeof(T);
            if (typeParameterType.Namespace != null && typeParameterType.Namespace.Contains("System.Collections.Generic"))
                ResultStatus = (((IList<T>)value).Count == 0) ? ResultStatus.DataNotFound : ResultStatus.Successful;
            else
                ResultStatus = (value == null) ? ResultStatus.DataNotFound : ResultStatus.Successful;
            Exception = null;
            Message = message;
        }

        /// <summary>
        /// برای مقدار دادن استفاده میگردد و مقدار درون این قرار میگیرد
        /// </summary>
        /// <param name="value"></param>
        /// <param name="recordsCount"></param>
        public void SetData(T value, long recordsCount)
        {
            Data = value;
            RecordsCount = recordsCount;
            var typeParameterType = typeof(T);
            if (typeParameterType.Namespace != null && typeParameterType.Namespace.Contains("System.Collections.Generic"))
                ResultStatus = (((IList<T>)value).Count == 0) ? ResultStatus.DataNotFound : ResultStatus.Successful;
            else
                ResultStatus = (value == null) ? ResultStatus.DataNotFound : ResultStatus.Successful;
            Exception = null;
            Message = "";
        }

        /// <summary>
        /// برای مقدار دادن پیام قرار میگیرد
        /// </summary>
        /// <param name="value"></param>
        /// <param name="recordsCount"></param>
        public void SetMessage(string message, ResultStatus resultStatus)
        {
            ResultStatus = resultStatus;
            Exception = null;
            Message = message;
        }

        public void SetException(string message)
        {
            ResultStatus = ResultStatus.Exception;
            Message = message;
        }

        public void ClearException()
        {
            Exception = null;
        }

        public void SetUnSuccessful(string message)
        {
            ResultStatus = ResultStatus.UnSuccessful;
            Exception = null;
            Message = message;
        }

        public void SetSuccessful()
        {
            ResultStatus = ResultStatus.Successful;
        }


        public static ServiceResponse<T> Success()
        {
            var res = new ServiceResponse<T>();
            res.SetSuccessful();
            return res;
        }

        public static ServiceResponse<T> Success(string message = "")
        {
            var res = new ServiceResponse<T>();
            res.SetMessage(message, ResultStatus.Successful);
            return res;
        }

        public static ServiceResponse<T> Success(T data)
        {
            var res = new ServiceResponse<T>();
            res.SetData(data);
            return res;
        }

        public static ServiceResponse<PagenatedList<T>> Success(List<T> data, long count)
        {
            var resultData = new PagenatedList<T>() { Total = count, Rows = data };
            return ServiceResponse<PagenatedList<T>>.Success(resultData);
        }

        public static ServiceResponse<T> Success(T data, string message = "")
        {
            var res = new ServiceResponse<T>();
            res.SetData(data);
            res.SetMessage(message, ResultStatus.Successful);
            return res;
        }

        public static ServiceResponse<T> Failed(T data, string message)
        {
            var res = new ServiceResponse<T>();
            res.SetData(data);
            res.SetMessage(message, ResultStatus.Exception);
            return res;
        }

        public static ServiceResponse<T> Failed(string message)
        {
            var res = new ServiceResponse<T>();
            res.SetMessage(message, ResultStatus.Exception);
            return res;
        }
    }
}
