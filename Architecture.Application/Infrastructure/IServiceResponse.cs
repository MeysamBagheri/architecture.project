﻿namespace Architecture.Application.Infrastructure
{
    public interface IServiceResponse
    {
        ResultStatus ResultStatus { get; set; }
        string Message { get; set; }
        object Data { get; set; }
    }
}