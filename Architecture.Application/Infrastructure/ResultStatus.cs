﻿namespace Architecture.Application.Infrastructure
{
    /// <summary>
    /// وضعیت اجرای دستور را در لایه سرویس مشخص میکند.
    /// </summary>
    public enum ResultStatus
    {
        /// <summary>
        /// دارای خطا و اجبار برای بروز کردن داده های قبلی
        /// </summary>
        ExceptionAndRebindData = -4,
        /// <summary>
        /// این مقدار نشاندهنده وجود استثناء در اجرای دستور است
        /// </summary>
        Exception = -3,
        /// <summary>
        /// در مراحل اجرای دستور، دیتای مورد نظر یافت نشد
        /// </summary>
        DataNotFound = -2,
        /// <summary>
        /// اجرای دستور، ناموفق بود
        /// </summary>
        UnSuccessful = -1,
        /// <summary>
        /// وضعیت پیش فرض است
        /// </summary>
        UnKnown = 0,
        /// <summary>
        /// دستور با موفقیت اجرا شد
        /// </summary>
        Successful = 1
    }
}
