﻿using Architecture.Application.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Architecture.Application.Validations
{
    public class UserValidator : AbstractValidator<UserCommandDto>
    {
        public UserValidator()
        {
            RuleFor(x => x.CreatedDate)
                .NotEmpty()
                .WithMessage("تاریخ را وارد کنید");
        }
    }
}
