﻿namespace Architecture.Application
{
    public class BaseViewModel<TKey> : IBaseViewModel
    {
        public long Id { get; set; }
    }
}
