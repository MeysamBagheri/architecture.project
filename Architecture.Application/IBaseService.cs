﻿using Architecture.Application.Infrastructure;
using Architecture.Core;
using Architecture.Core.Enum;
using Architecture.Domain;
using FluentValidation;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Architecture.Application
{
    public interface IBaseService<TEntity, TKey, TCommand, TViewModel>
        where TCommand : IBaseCommand
        where TViewModel : IBaseViewModel
        where TEntity : IAggregateRoot
    {
        Task<ServiceResponse<PagenatedList<TViewModel>>> GetAsync(SimpleQuery simpleQuery);

        Task<ServiceResponse<TViewModel>> GetByIdAsync(TKey id);

        Task<ServiceResponse<TKey>> AddAsync(TCommand command, long userId);

        Task<ServiceResponse<long>> UpdateAsync(TCommand command, long userId);

        Task<ServiceResponse<bool>> ActiveAsync(TKey id, long userId);

        Task<ServiceResponse<bool>> DeActiveAsync(TKey id, long userId);

        Task<ServiceResponse<bool>> DeleteAsync(TKey id, DeletingType deletingType, long userId);

    }
}